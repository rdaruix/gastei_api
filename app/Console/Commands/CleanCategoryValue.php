<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use DB;
use Modules\Categories\Entities\Category;
use Modules\ConsolidarMes\Entities\ConsolidarMes;

class CleanCategoryValue extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clean:category';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $categories = Category::all();
        $dt = Carbon::now();
        $actualMonth = $dt->month;
        $monthBase = DB::table('consolidate_month')->where('month','=', $actualMonth)->exists();

        $totalValue = 0;
        foreach ( $categories as $category) {
            $totalValue += $category->total_value + $category->total_value_year;
            DB::table('categories')->where('id', $category->id)->update(['total_value_year' => $category->total_value + $category->total_value_year]);
        }

        if (!$monthBase) {
            DB::table('consolidate_month')->insertGetId(['month' => $actualMonth, 'total_value_month' => $totalValue]);
        } else {
            DB::table('consolidate_month')->where('month', $actualMonth)->update(['total_value_month' => $totalValue]);
        }

        DB::table('categories')->update(['total_value' => 0]);
    }

}
