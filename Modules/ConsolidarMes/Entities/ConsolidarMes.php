<?php

namespace Modules\ConsolidarMes\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ConsolidarMes extends Model
{
    protected $fillable = ['id', 'month', 'total_value_month'];
    use SoftDeletes;
    protected $dates = ['deleted_at'];
}
