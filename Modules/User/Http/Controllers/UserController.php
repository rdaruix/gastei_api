<?php

namespace Modules\User\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Modules\User\Entities\User;
use JWTAuth;
use Modules\User\Entities\Transformers\UserTransform;
use Spatie\Fractalistic\Fractal;
use Spatie\Fractalistic\ArraySerializer as Serializer;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('user::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('user::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('user::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('user::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    public function doLogin(Request $request){

        $userLogin = User::where('email', $request['email'])->first();
        if($userLogin){
            $token = JWTAuth::fromUser($userLogin);
            return response()->json([
                'status' => 200,
                'slug' => 'response-ok',
                'message' => 'Response ok.',
                'token' => 'Bearer '.$token,
                'userLogged' => $userLogin['email']
            ], 200);
        }else{

            return response()->json([
                'status' => 400,
                'slug' => 'without-user',
                'message' => 'No user found'
            ], 400);
        }

    }

    public function getUsers(){

        $data = User::all();

        $usersFormatted = Fractal::create()
            ->collection($data)
            ->transformWith(\Modules\User\Entities\Transformers\UserTransform::class)
            ->serializeWith(new Serializer());

        return response()->json([
            'status' => 200,
            'slug' => 'response-ok',
            'message' => 'Response ok.',
            'data' => $usersFormatted
        ], 200);
    }

    public function getUser(){

        $user = JWTAuth::parseToken()->authenticate();
        $account = User::where('email', $user['email'])->first();

        try {

            if(is_null($account)){

                return response()->json([
                    'status' => 400,
                    'slug' => 'account-not-found',
                    'message' => 'Account not found.',
                ], 400);
            }

        } catch (TokenExpiredException $e) {

            return response()->json([
                'status' => 400,
                'slug' => 'token-has-expired',
                'message' => 'Token has expired',
            ], 400);

        } catch (TokenInvalidException $e){
            return response()->json([
                'status' => 400,
                'slug' => 'token-is-invalid',
                'message' => 'Token is invalid',
            ], 400);
        }catch (JWTException $e){
            return response()->json([
                'status' => 400,
                'slug' => 'token-not-exists',
                'message' => 'Token not exists',
            ], 400);
        }

        $userFormatted = Fractal::create()
            ->item($account)
            ->transformWith(UserTransform::class)
            ->serializeWith(new Serializer())
            ->toArray();
        return response()->json($userFormatted, 200);
    }
}
