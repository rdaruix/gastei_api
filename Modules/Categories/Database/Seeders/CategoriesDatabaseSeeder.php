<?php

namespace Modules\Categories\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Categories\Entities\Category;

class CategoriesDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {        
        Category::create(['description'=>'Crédito', 'total_value'=> 0]);
        Category::create(['description'=>'Descontos', 'total_value'=> 0]);
        Category::create(['description'=>'Despesas Diárias', 'total_value'=> 0]);
        Category::create(['description'=>'Despesas Mensais', 'total_value'=> 0]);
    }
}
