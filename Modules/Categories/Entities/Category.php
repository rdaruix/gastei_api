<?php

namespace Modules\Categories\Entities;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $guard_name = 'web';

    protected $fillable = ['id', 'description', 'total_value', 'total_value_year'];

    use SoftDeletes;
    protected $dates = ['deleted_at'];
}
