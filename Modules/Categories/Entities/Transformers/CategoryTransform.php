<?php

namespace Modules\Categories\Entities\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use League\Fractal;
use Modules\Categories\Entities\Category;


class CategoryTransform extends Fractal\TransformerAbstract
{
    public function transform(Item $item){
        return [
            'id'               => (int) $item->id,
            'description'      => $item->description,
            'total_value'      => $item->total_value,
            'total_value_year' => $item->total_value_year,
            'created_at'       => $item->created_at,
            'updated_at'       => $item->updated_at
        ];
    }
}
