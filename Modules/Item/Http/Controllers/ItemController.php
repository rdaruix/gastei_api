<?php

namespace Modules\Item\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Item\Entities\Item;
use Spatie\Fractalistic\Fractal;
use Spatie\Fractalistic\ArraySerializer as Serializer;
use Modules\Categories\Entities\Category;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('item::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function addItem(Request $request)
    {
        $item = $request->all();
        $categoriaItem = Category::where('id', $request['categoria_id'])->first();
        if($categoriaItem){
            Item::create($item);
            $categoriaItem->total_value = $categoriaItem['total_value'] + $item['value'];
            $categoriaItem->save();
            return response()->json([
                'status' => 200,
                'slug' => 'response-ok',
                'message' => $categoriaItem
            ], 200);

        }else{
            return response()->json([
                'status' => 400,
                'slug' => 'without-category',
                'message' => 'No category found'
            ], 400);
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('item::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('item::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
