<?php

namespace Modules\Item\Entities;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $guard_name = 'web';

    protected $fillable = ['id', 'description', 'value'];

    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function getCategory(){
        return $this->belongsTo('Modules\Categories\Entities\Category', 'category_id');
    }
}
