<?php

namespace Modules\Item\Entities\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use League\Fractal;
use Modules\Item\Entities\Item;


class ItemTransform extends Fractal\TransformerAbstract
{
    public function transform(Item $item){
        return [
            'id'           => (int) $item->id,
            'description'  => $item->description,
            'value'        => $item->value,
            'category'     => $item->getCategory()->getEager()->first(),
            'created_at'   => $item->created_at,
            'updated_at'   => $item->updated_at
        ];
    }
}
